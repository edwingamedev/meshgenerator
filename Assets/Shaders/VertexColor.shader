﻿Shader "Custom/VertexColor"
{
	SubShader
	{
		// Start	
		CGPROGRAM

		#pragma surface surf Lambert

		// Naming this input is just convention
		struct Input
		{
			float4 vertColour : COLOR;
		};
		
		void surf(Input IN, inout SurfaceOutput o)
		{
			// set the mesh's albedo to be the vertex color
			o.Albedo = IN.vertColour;
		}

		ENDCG
	}


	FallBack "Diffuse"
}
