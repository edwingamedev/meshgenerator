﻿using UnityEngine;

namespace EdwinGameDev
{
	public class TriangleGenerator : AbstractMeshGenerator
	{
		[SerializeField] private Vector3[] vs = new Vector3[3];
		[SerializeField] private bool reverseTriangle;

		protected override void SetMeshNums()
		{
			//both numbers are 3 for a single triangle.
			numVertices = 3;
			numTriangles = 3;
		}

		protected override void SetVertices()
		{
			base.vertices.AddRange(vs);
		}

		protected override void SetTriangles()
		{
			if (!reverseTriangle)
			{
				triangles.Add(0);
				triangles.Add(1);
				triangles.Add(2);
			}
			else
			{
				triangles.Add(0);
				triangles.Add(2);
				triangles.Add(1);
			}
		}

		//unused for now
		protected override void SetNormals() { }
		protected override void SetTangents() { }
		protected override void SetUVs() { }
		protected override void SetVertexColours() { }
	}

}